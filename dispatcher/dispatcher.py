# coding: utf-8
import logging
import re
import functools


from .errors import BadCommand, ArgumentsParseError


logger = logging.getLogger(__name__)


class CommandsDispatcher(object):

    def __init__(self, mapping=None, fabrics=None, api=None):
        self.mapping = mapping or []
        self.fabrics = fabrics or {'int': int}
        self.api = api

    def run(self, message):
        func, kwargs = self.dispatch(message)
        return func(**kwargs)

    def dispatch(self, message):
        for pattern, func in self.mapping:
            parts = pattern.split(' ')
            command = parts[0]
            arguments_desc = parts[1:]
            if message.startswith(command):
                logger.debug("Found pattern %s for message %s",
                             pattern,
                             message)
                if arguments_desc:
                    return func, self.parse_command_arguments(
                        pattern,
                        message)
                return func, {}
        raise BadCommand("Bad command %s" % message.split()[0])

    def parse_command_arguments(self, pattern, command):
        result = {}
        try:
            command_parts = command.split(" ", 1)[1].split()
            arguments = pattern.split(" ", 1)[1].split()
            assert len(command_parts) == len(arguments)
        except (IndexError, AssertionError):
            raise ArgumentsParseError("Bad command arguments %s" % command)
        fabric = str
        for idx, argument_description in enumerate(arguments):
            fabric, name, pattern = self.parse_argument_description(
                argument_description
            )
            if re.match(pattern, command_parts[idx]):
                result[name] = fabric(command_parts[idx])
        return result

    def parse_argument_description(self, argument_description):
        try:
            fabric = str
            spec_parts = argument_description[1:-1].split(':')
            assert len(spec_parts) in (2, 3)
            if len(spec_parts) == 2:
                name, pattern = spec_parts
            if len(spec_parts) == 3:
                name, pattern, fabric_name = spec_parts
                fabric = self.fabrics[fabric_name]
            return fabric, name, pattern
        except (AssertionError, KeyError):
            raise ArgumentsParseError("Bad argument description format %s",
                                      argument_description)

    def command(self, cmd):
        def inner_wrapper(func):
            self.mapping.append((cmd, func))
            @functools.wraps(func)
            def wrapper(*arg, **kwargs):
                return func(*arg, **kwargs)
            return wrapper
        return inner_wrapper

    def handle(self, message):
        answer = self.run(message['text'])
        self.send_answer(message["chat_id"], answer, message)

    def send_answer(self, chat_id, answer, message=None):
        self.api.sendMessage(chat_id, text=answer)

    def handle_update_id(self, update_id):
        pass
