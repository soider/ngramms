class ArgumentsParseError(Exception):
    pass

class BadCommand(Exception):
    pass
