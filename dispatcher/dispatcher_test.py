import dispatcher
import pytest


def help_cmd():
    return "/help will return this text"


def args_cmd(first_argument, second_argument):
    pass


def test_obvious_dispatching():
    disp = dispatcher.CommandsDispatcher(
        [('/help', help_cmd)]
    )
    assert disp.dispatch("/help") == (help_cmd, {})


def test_obvious_dispatching_with_args():
    disp = dispatcher.CommandsDispatcher(
        [('/help <first_argument:\w+> <second_argument:\d+:int>', help_cmd)]
    )
    assert disp.dispatch("/help 1 2") == (help_cmd, {'first_argument': '1',
                                                     'second_argument': 2})


def test_parse_command():
    command = "/echo first_argument 100"
    pattern = "/echo <first_argument:\w+> <second_argument:\d+>"
    disp = dispatcher.CommandsDispatcher(
        None
    )
    assert disp.parse_command_arguments(
        pattern, command
    ) == {'first_argument': 'first_argument',
          'second_argument': '100'}


def test_parse_command_with_type_spec():
    command = "/echo 100"
    pattern = "/echo <arg:\d+:long>"
    disp = dispatcher.CommandsDispatcher(
        None, {'long': long}
    )
    assert disp.parse_command_arguments(
        pattern, command
    ) == {'arg': 100L}


def test_parse_command_wrong_numbers():
    command = "/echo field1 field2"
    pattern = "/echo <arg:\d+:long>"
    disp = dispatcher.CommandsDispatcher(
        None, {'long': long}
    )
    with pytest.raises(dispatcher.ArgumentsParseError):
        disp.parse_command_arguments(
            pattern, command)


def test_parse_argument_spec():
    builder = lambda e: e
    arg_name = "arg"
    pattern = "\d+"
    arg_spec = "<%s:%s:builder>" % (arg_name, pattern)
    disp = dispatcher.CommandsDispatcher(
        None, {'builder': builder}
    )
    assert disp.parse_argument_description(arg_spec) == (builder,
                                                         arg_name,
                                                         pattern)


def test_broken_argument_spec():
    with pytest.raises(dispatcher.ArgumentsParseError):
        dispatcher.CommandsDispatcher().parse_argument_description(
            "<asd>"
        )


def test_command_decorator():
    disp = dispatcher.CommandsDispatcher()
    cmd = '/echo'

    @disp.command(cmd)
    def echo_command():
        1 / 0

    assert disp.mapping[0][0] == cmd
    with pytest.raises(ZeroDivisionError):
        disp.mapping[0][1]()
