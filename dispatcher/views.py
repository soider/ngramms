from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .errors import BadCommand

# Create your views here.


def generic_view(dispatcher):
    @csrf_exempt
    def inner(request):
        try:
            data = json.loads(request.body)
        except ValueError:
            return HttpResponse("No json found", 400)
        try:
            message = data['message']
            update_id = data['update_id']
            answer = dispatcher.handle(message)
            dispatcher.handle_update_id(update_id)
            return HttpResponse("OK")
        except KeyError:
            return HttpResponse("Invalid output from telegram", 400)
        except BadCommand:
            return HttpResponse("Invalid command", 400)
    return inner
