# coding: utf-8
from __future__ import unicode_literals
import string
import random
from collections import defaultdict


def build_ngramm_stats(text, dimension):
    tokens = tokenize(text)
    ngramms = lap(tokens, dimension + 1)
    index = defaultdict(lambda: defaultdict(int))
    for ngram in ngramms:
        key = ngram[:dimension]
        word = ngram[dimension]
        index[key][word] += 1
    index = dict(index)
    for key, value in index.items():
        index[key] = dict(value)
    return index


def tokenize(text):
    """Tokenize given :text:

    @param :text: string
    @return list of strings
    """
    result = []
    for potential_token in text.split():
        sanitized = potential_token.strip(string.punctuation).strip().lower()
        if sanitized:
            result.append(sanitized)
    return result


def lap(iterable, window_size):
    """Split :iterable: into overlapping windows size of :window_size:

    @param :iterable: any iterable of tuples
    @param :window_size: window size
    @return list
    """
    for idx, element in enumerate(iterable):
        part = tuple(iterable[idx:idx + window_size])
        if len(part) == window_size:
            yield part


def choice(iterable, get_weight=lambda element: element[1]):
    """Weighted random choice from :iterable:

    @param :iterable: any iterable for choice from
    @param :get_weight: weight getter
    @return any
    """
    sum_of_all_weights = sum([get_weight(element) for element in iterable])
    counter = 0
    while counter <= sum_of_all_weights:
        choiced = random.choice(iterable)
        counter += get_weight(choiced)
    return choiced


def generate(index, length, max_word_retries=10):
    """Generate text length of :length: based on markov ngramm :index:

    @param :index: dict with ngramms statistic
    @param :length: final text length
    """
    initial = random.choice(index.keys())
    result = []
    for part in initial:
        result.append(part)
    ngramm = initial
    count = 0
    while True:
        if len(result) >= length:
            break
        if ngramm not in index:
            count += 1
            if count >= max_word_retries:
                break 
            continue
        else:
            new_word = choice(index[ngramm].items())[0]
            ngramm = (ngramm[1], new_word)
            result.append(new_word)
    return str(' ').join(result)


if __name__ == "__main__":
    import sys
    try:
        ngramm_length, text_length, source_file = sys.argv[1:]
        ngramm_length = int(ngramm_length)
        text_length = int(text_length)
    except ValueError:
        sys.exit("Usage: %s <ngramm_length> <text_length> <file>" %
                 sys.argv[0])
    else:
        index = build_ngramm_stats(open(source_file).read().decode('utf-8'),
                                   ngramm_length)
        print generate(index, text_length)
