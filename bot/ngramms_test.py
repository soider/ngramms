import ngramms
import pytest


sentence = "This is a good choice to test my ngramm-builder ten"


def test_bigrams_stats():
    expected_bigramm = {
        ('this', 'is'): {'a': 1},
        ('is', 'a'): {'good': 1},
        ('a', 'good'): {'choice': 1},
        ('good', 'choice'): {'to': 1},
        ('choice', 'to'): {'test': 1},
        ('to', 'test'): {'my': 1},
        ('test', 'my'): {'ngramm-builder': 1},
        ('my', 'ngramm-builder'): {'ten': 1}
    }
    assert ngramms.build_ngramm_stats(sentence, 2) == expected_bigramm


def test_tokenize():
    raw_text = "This is text, , listen to me, it's okay"
    assert ngramms.tokenize(raw_text) == ["this", "is", "text",
                                          "listen", "to", "me", "it's",
                                          "okay"]


def test_lap_iterable_2_dimension():
    iterable = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    assert list(ngramms.lap(iterable, 2)) == [(1, 2),
                                              (2, 3),
                                              (3, 4),
                                              (4, 5),
                                              (5, 6),
                                              (6, 7),
                                              (7, 8),
                                              (8, 9),
                                              (9, 10)]


def test_lap_iterable_3_dimension():
    iterable = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    assert list(ngramms.lap(iterable, 3)) == [
        (1, 2, 3), (2, 3, 4), (3, 4, 5), (4, 5, 6), (5, 6, 7), (6, 7, 8), (7, 8, 9), (8, 9, 10)]
