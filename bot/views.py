from dispatcher.dispatcher import CommandsDispatcher
from dispatcher.views import generic_view
import telegram
import requests
from BeautifulSoup import BeautifulSoup
import sys
import ngramms


api = telegram.Bot(token='47699987:AAFgF0jEi8L4z6JPhkjyjmlLR9vU0bLSlUs')

disp = CommandsDispatcher(api=api)


@disp.command("/generate <url:\w+>")
def echo(url):
    try:
        resp = requests.get(url)
        resp.raise_for_status()
        body = resp.content
    except requests.RequestException:
        return "Shit happened :("
    soup = BeautifulSoup(body)
    all_text = ''.join([el.text for el in
                        soup.findAll('div', attrs={'class': 'quotbody'})])
    index = ngramms.build_ngramm_stats(all_text.encode('utf-8'),
                               2)
    print all_text
    return ngramms.generate(index, 100)
    return "Will generate stuff from %s" % url


dispatcher_view = generic_view(disp)
