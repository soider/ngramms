import dispatcher


disp = dispatcher.CommandsDispatcher(fabrics={'long': long, 'int': int})


@disp.command('/help')
def help_cmd():
    return "Help commands output"


@disp.command('/say_my_name <name:\w+>')
def name_cmd(name):
    return "Say my name, %s" % name


@disp.command('/multiply <operand:\d+:long> <operand2:\d+:long>')
def mult_cmd(operand, operand2):
    return operand * operand2
